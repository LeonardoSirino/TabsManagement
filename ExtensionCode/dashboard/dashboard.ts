class TM_Session {
    date: Date
    name: string
    hash: string
    unsavedChanges: boolean
    windows: Array<TM_Window>
    constructor(date: Date) {
        this.date = date
        this.name = 'Untitled'
        this.windows = new Array()
        this.hash = null
        this.unsavedChanges = false
    }

    replaceWindows(originSession: TM_Session) {
        this.unsavedChanges = true
        this.windows = originSession.windows
    }

    pushGenericObject(generic: any) {
        this.date = generic.date
        this.name = generic.name
        this.hash = generic.hash
        let k = 1
        generic.windows.forEach(window => {
            let newWindow = new TM_Window(k)
            window.tabs.forEach(tab => {
                newWindow.addTab(tab)
            })
            this.addWindow(newWindow)
            k++
        })
    }

    addWindow(window: TM_Window) {
        this.windows.push(window)
    }

    openTabs(mode: string) {
        if (mode === '1') {
            this.windows.forEach((window) => {
                window.tabs.forEach((tab) => {
                    let createProperties = {
                        'url': tab.url
                    }

                    chrome.tabs.create(createProperties)
                })
            })
        } else if (mode === '2') {
            let URLs = []
            this.windows.forEach((window) => {
                window.tabs.forEach((tab) => {
                    URLs.push(tab.url)
                })
            })

            let objecData = {
                'url': URLs
            }

            chrome.windows.create(objecData)
        } else if (mode === '3') {
            this.windows.forEach((window) => {
                let URLs = []
                window.tabs.forEach((tab) => {
                    URLs.push(tab.url)
                })
                let objecData = {
                    'url': URLs
                }

                chrome.windows.create(objecData)
            })
        }
    }

    render(containerDiv: HTMLElement) {
        let iW = 0
        this.windows.forEach((window) => {
            let tabs = window.tabs

            let windowOptions = createWindowOptions()

            let windowDiv = document.createElement('div')
            windowDiv.className = 'window'

            let windowTitleDiv = document.createElement('div')
            windowTitleDiv.className = 'windowTitle'
            windowTitleDiv.innerText = 'Window ' + (+iW + 1)
            iW++
            windowDiv.appendChild(windowTitleDiv)

            for (let iT in tabs) {
                let tab = tabs[iT]
                let tabDiv = document.createElement('a')
                tabDiv.className = 'tab'

                let tabIcon = document.createElement('img')
                tabIcon.className = 'icon'
                if (tab.icon !== "") {
                    tabIcon.setAttribute('src', tab.icon)
                } else {
                    tabIcon.setAttribute('src', "../images/icon.png")
                    console.log('Imagem não existe')
                    console.log("../images/icon.png")
                }

                let tabTitle = document.createElement('a')
                tabTitle.className = 'tabTitle'
                tabTitle.innerText = tab.title

                let tabURL = document.createElement('a')
                tabURL.className = 'tabURL'
                tabURL.innerText = tab.url

                let tabTitleandURL = document.createElement('a')
                tabTitleandURL.className = 'tabTitleandURL'
                tabTitleandURL.appendChild(tabTitle)
                tabTitleandURL.appendChild(tabURL)
                tabTitleandURL.onclick = ((e) => {
                    console.log(e); // Identificar como observar os eventos com os outros botões do mouse
                    let element = e.srcElement
                    let k = 0
                    while (element.className !== 'tabTitleandURL') {
                        element = element.parentNode
                        k++
                        if (k > 10) {
                            break
                        }
                    }
                    let url = element.getElementsByClassName('tabURL')[0].innerText
                    let createProperties = {
                        'url': url,
                        'active': false
                    }

                    chrome.tabs.create(createProperties)
                })

                let deleteDiv = document.createElement('button')
                deleteDiv.className = 'deleteDiv'
                deleteDiv.innerText = 'X'
                deleteDiv.onclick = ((e) => {
                    let parent = e.srcElement.parentNode
                    let url = parent.getElementsByClassName('tabTitleandURL')[0].getElementsByClassName('tabURL')[0].innerText
                    let windowName = parent.parentNode.getElementsByClassName('windowTitle')[0].innerText

                    removeTab(activeSession, windowName, url)
                    console.log(activeSession)
                    buildDashboard(activeSession)
                })

                tabDiv.appendChild(deleteDiv)
                tabDiv.appendChild(tabIcon)
                tabDiv.appendChild(tabTitleandURL)
                tabDiv.appendChild(document.createElement('br'))

                document.getElementsByClassName

                tabDiv.onmouseenter = ((e) => {
                    let parent = e.srcElement
                    parent.getElementsByClassName('deleteDiv')[0].style.display = 'block'
                })

                tabDiv.onmouseleave = ((e) => {
                    let parent = e.srcElement
                    parent.getElementsByClassName('deleteDiv')[0].style.display = 'none'
                })

                windowDiv.appendChild(tabDiv)
            }

            windowDiv.appendChild(windowOptions)

            containerDiv.appendChild(windowDiv)
        })
    }

    saveToCloud() {
        if (this.hash == null) {
            this.hash = (+new Date).toString(36)
        }

        this.unsavedChanges = false
        this.name = getSessionName()
        this.date = new Date()

        let requestData = {
            'operation': 'saveSession',
            'data': JSON.stringify(this)
        }

        console.log('Salvando sessão')
        console.log(this)

        clearSessionsDiv();
        let sessionsDiv = document.getElementsByClassName('sessionsDiv')[0]
        let loadingGif = document.createElement('iframe')
        loadingGif.className = 'loadingGif'
        loadingGif.setAttribute('src', '../images/loading.gif')
        sessionsDiv.appendChild(loadingGif)

        let plainRequestData = JSON.stringify(requestData)
        const Http = new XMLHttpRequest();
        const url = 'https://script.google.com/macros/s/AKfycbyc9bb49ol9O_R6zZz6c8SnouMS6_NrVS79DHtTi5Mb0KP6sds/exec';
        Http.open("POST", url);
        Http.send(plainRequestData);
        Http.onreadystatechange = (e) => {
            let readyState = e.currentTarget.readyState
            if (readyState != 4) {
                console.log('Ainda não concluido. Status: ')
                console.log(readyState)
                return 'Not done yet'
            }
            alreadyBuilt = false;
            getAllSessions(); // A construção do painel de sessões já é solicitada na execução dessa função
            markFirstSession = true // O painel é construido de maneira assincrona, por isso é definido um flag para marcar a primeira sessão
        }
    }

    delete() {
        let requestData = {
            'operation': 'deleteSession',
            'data': JSON.stringify(this)
        }

        console.log('Deletando sessão')
        console.log(this)

        clearSessionsDiv();
        let sessionsDiv = document.getElementsByClassName('sessionsDiv')[0]
        let loadingGif = document.createElement('iframe')
        loadingGif.className = 'loadingGif'
        loadingGif.setAttribute('src', '../images/loading.gif')
        sessionsDiv.appendChild(loadingGif)

        let plainRequestData = JSON.stringify(requestData)
        const Http = new XMLHttpRequest();
        const url = 'https://script.google.com/macros/s/AKfycbyc9bb49ol9O_R6zZz6c8SnouMS6_NrVS79DHtTi5Mb0KP6sds/exec';
        Http.open("POST", url);
        Http.send(plainRequestData);
        Http.onreadystatechange = (e) => {
            let readyState = e.currentTarget.readyState
            if (readyState != 4) {
                console.log('Ainda não concluido. Status: ')
                console.log(readyState)
                return 'Not done yet'
            }
            console.log('Payload de envio')
            console.log(plainRequestData)
            getAllSessions();
        }
    }
}

class TM_Window {
    id: string
    tabs: Array<any>
    constructor(id) {
        this.id = id
        this.tabs = new Array()
    }

    addTab(tab) {
        this.tabs.push(tab)
    }
}

let activeSession = new TM_Session(new Date())
let serverSessions = []
let markFirstSession = false
let alreadyBuilt = false

function scanCurrentSession() {
    let session = new TM_Session(new Date())
    // let myWindows = []
    chrome.windows.getAll({ populate: true }, function (windows) {
        windows.forEach(function (window) {
            // let myTabs = new Array()
            let myWindow = new TM_Window(window.id)

            window.tabs.forEach(function (tab) {
                let myTab = {
                    'title': tab.title,
                    'url': tab.url,
                    'icon': tab.favIconUrl,
                    'status': tab.status
                }

                if (tab.title !== 'TabsManagement') {
                    myWindow.addTab(myTab)
                }
            });
            session.addWindow(myWindow)
        });
    });

    return session
}

function initPage() {
    // Evento para identificar saída da página sem salvar
    window.addEventListener('beforeunload', (e) => {
        console.log(e);
        if (activeSession.unsavedChanges) {
            e.preventDefault();
            e.returnValue = '';
        }
    });

    let mainDiv = document.createElement('div')
    mainDiv.className = 'mainDiv'

    let sessionsDiv = document.createElement('div')
    sessionsDiv.className = 'sessionsDiv'

    let allWindowsDiv = document.createElement('div')
    allWindowsDiv.className = 'allWindowsDiv'

    mainDiv.appendChild(sessionsDiv)
    mainDiv.appendChild(allWindowsDiv)
    document.body.appendChild(mainDiv)

    clearSessionsDiv()

    let loadingGif = document.createElement('iframe')
    loadingGif.className = 'loadingGif'
    loadingGif.setAttribute('src', '../images/loading.gif')
    sessionsDiv.appendChild(loadingGif)
}

function clearSessionsDiv() {
    // Falta limpar o botão de autorização
    let sessionsDiv = document.getElementsByClassName('sessionsDiv')[0]
    sessionsDiv.innerHTML = ''

    let logoDiv = document.createElement('img')
    logoDiv.setAttribute('src', '../images/logo_text.png')
    logoDiv.className = 'logoDiv'
    logoDiv.onclick = (e) => {
        let createProperties = {
            'url': 'https://gitlab.com/LeonardoSirino/TabsManagement'
        }

        chrome.tabs.create(createProperties)
    }
    sessionsDiv.appendChild(logoDiv)
}

function buildSessionsCards(sessions) {
    clearSessionsDiv()

    if (sessions.length !== 0) {
        let sessionsDiv = document.getElementsByClassName('sessionsDiv')[0]

        let sessionsCards = document.createElement('div')
        sessionsCards.className = 'sessionsCardsContainer'
        sessions.forEach((session) => {
            let sessionCard = document.createElement('div')
            sessionCard.className = 'cardSession'

            let sessioNameDiv = document.createElement('div')
            sessioNameDiv.className = 'cardName'
            sessioNameDiv.innerText = session.name

            let sessionCardStats = document.createElement('div')
            sessionCardStats.className = 'cardStats'

            let sessionCardNumbers = document.createElement('div')
            sessionCardNumbers.className = 'cardNumbers'
            let numWindows = session.windows.length
            let numTabs = 0

            session.windows.forEach(window => {
                numTabs += window.tabs.length
            });

            sessionCardNumbers.innerText = numWindows + ' windows\n' + numTabs + ' tabs'

            let sessionCardDate = document.createElement('div')
            sessionCardDate.className = 'cardDate'
            let sessionDate = new Date(session.date)
            sessionCardDate.innerText = sessionDate.toLocaleDateString('pt-BR')

            sessionCard.appendChild(sessioNameDiv)
            sessionCardStats.appendChild(sessionCardNumbers)
            sessionCardStats.appendChild(sessionCardDate)
            sessionCard.appendChild(sessionCardStats)

            let hashDiv = document.createElement('div')
            hashDiv.className = 'hashDiv'
            hashDiv.innerText = session.hash
            hashDiv.style.display = 'None'
            sessionCard.appendChild(hashDiv)

            sessionCard.onclick = (e) => {
                if (activeSession.unsavedChanges) {
                    let aux = confirm("There are unsaved changes on the open session. If you leave, this changes won't be saved. Do you really want to leave?")
                    if (aux) {
                        console.log('Alterações descartadas');
                    } else {
                        return null
                    }
                }
                let element = e.srcElement
                let k = 0
                while (element.className !== 'cardSession') {
                    element = element.parentNode
                    k++
                    if (k > 10) {
                        break
                    }
                }

                try {
                    let previousSelected = element.parentNode.getElementsByClassName('cardSessionSelected')[0]
                    previousSelected.className = 'cardSession'
                } catch{
                    console.log('Erro no previousSelected')
                }

                element.className = 'cardSessionSelected'

                let hash = element.getElementsByClassName('hashDiv')[0].innerText
                let sessionData = getSessionbyHash(hash)
                buildDashboard(sessionData)
            }

            sessionsCards.appendChild(sessionCard)
        })

        sessionsDiv.appendChild(sessionsCards)
    } else {
        let sessionsDiv = document.getElementsByClassName('sessionsDiv')[0]

        let message = document.createElement('message')
        message.className = 'noSession'
        message.innerText = "No session saved so far"

        sessionsDiv.appendChild(message)
    }

    if (markFirstSession) {
        // Set do foco na sessão recém salva
        try {
            let previousSelected = document.getElementsByClassName('cardSessionSelected')[0]
            previousSelected.className = 'cardSession'
        } catch{
            console.log('Erro no previousSelected')
        }

        console.log('Tentando setar a classe do card selecionado');
        console.log(document.getElementsByClassName('cardSession').item(0));

        document.getElementsByClassName('cardSession').item(0).className = 'cardSessionSelected'

        markFirstSession = false
    }
}

function buildDashboard(session) {
    activeSession = session

    let allWindowsDiv = document.getElementsByClassName('allWindowsDiv')[0]
    allWindowsDiv.innerHTML = ''

    if (activeSession.windows.length === 0) {
        let emptyText = document.createElement('div')
        emptyText.className = 'emptyText'
        emptyText.innerText = 'There is no windows in this session, please select other'
        allWindowsDiv.appendChild(emptyText)
        return
    }

    let windows = session.windows

    let name = session.name
    if (!name) {
        name = 'Untitled'
    }

    let header = document.createElement('div')
    header.className = 'dashHeader'

    let sessionNameHolder = document.createElement('form')
    sessionNameHolder.setAttribute('autocomplete', 'off')
    let sesssionName = document.createElement('input')
    sesssionName.setAttribute('autocomplete', 'false')
    sesssionName.setAttribute('name', 'hidden')
    sesssionName.onchange = ((e) => {
        activeSession.unsavedChanges = true
    })

    sesssionName.id = 'sessionName'
    sesssionName.value = name

    sessionNameHolder.appendChild(sesssionName)
    header.appendChild(sessionNameHolder)

    let saveButton = document.createElement('button')
    saveButton.className = 'headerButton'
    saveButton.innerText = 'Save'
    saveButton.onclick = (() => { activeSession.saveToCloud() })

    let delButton = document.createElement('button')
    delButton.className = 'headerButton'
    delButton.innerText = 'Delete'
    delButton.onclick = ((e) => {
        let aux = confirm('Do you really want to delete this session?')
        if (aux) [
            activeSession.delete()
        ]
    })

    let overwriteButton = document.createElement('button')
    overwriteButton.className = 'headerButton'
    overwriteButton.innerText = 'Overwrite with current'
    overwriteButton.onclick = (() => {
        let currentSession = scanCurrentSession()
        // Também estudar como remover esse timeout
        setTimeout(() => {
            activeSession.replaceWindows(currentSession)
            buildDashboard(activeSession)
        }, 200)
    })

    let openButton = document.createElement('button')
    openButton.className = 'headerButton'
    openButton.innerText = 'Open'

    let dropMenu = document.createElement('div')
    dropMenu.className = 'headerDropMenu'

    dropMenu.appendChild(openButton)

    let dropMenuContent = document.createElement('div')
    dropMenuContent.className = 'dropMenuContent'

    let dmb1 = document.createElement('Button')
    dmb1.className = 'dropMenuItem'
    dmb1.innerText = 'Open tabs\nin this window'
    dmb1.onclick = ((e) => {
        console.log(activeSession)
        activeSession.openTabs('1')
    })

    let dmb2 = document.createElement('Button')
    dmb2.className = 'dropMenuItem'
    dmb2.innerText = 'Open tabs\nin new window'
    dmb2.onclick = ((e) => {
        console.log(activeSession)
        activeSession.openTabs('2')
    })

    let dmb3 = document.createElement('Button')
    dmb3.className = 'dropMenuItem'
    dmb3.innerText = 'Open tabs\nin respective windows'
    dmb3.onclick = ((e) => {
        console.log(activeSession)
        activeSession.openTabs('3')
    })

    dropMenuContent.appendChild(dmb1)
    dropMenuContent.appendChild(dmb2)
    dropMenuContent.appendChild(dmb3)

    dropMenu.appendChild(dropMenuContent)

    let headerButtonsHolder = document.createElement('div')
    headerButtonsHolder.className = 'headerButtonsHolder'

    headerButtonsHolder.appendChild(dropMenu)
    headerButtonsHolder.appendChild(saveButton)
    headerButtonsHolder.appendChild(delButton)
    headerButtonsHolder.appendChild(overwriteButton)

    header.appendChild(headerButtonsHolder)

    allWindowsDiv.appendChild(header)

    let hashDiv = document.createElement('div')
    hashDiv.innerText = session.hash
    hashDiv.style.display = 'None'

    allWindowsDiv.appendChild(hashDiv)
    session.render(allWindowsDiv)
}

function getSessionbyHash(hash: string) {
    let matched = new TM_Session(new Date())
    serverSessions.forEach(e => {
        if (e.hash === hash) {
            matched = e
        }
    })

    return matched
}

function removeTab(session: TM_Session, windowName: string, url: string) {
    session.unsavedChanges = true
    let j = 0
    session.windows.forEach((window) => {
        let k = 0
        window.tabs.forEach((tab) => {
            // Tirar isso daqui
            let data = {
                curName: window.id,
                searchName: windowName,
                curURL: tab.url,
                searchURL: url
            }
            console.table(data)
            if (tab.url === url) {
                let aux = window.tabs.splice(k, 1)
                console.log(aux)
            }
            k++
        })
        if (window.tabs.length === 0) {
            session.windows.splice(j, 1)
        }
        j++
    })
}

function getAllSessions() {
    let requestData = {
        'operation': 'getAllSesssions',
        'data': ''
    }

    let alreadyBuilt = false

    let plainRequestData = JSON.stringify(requestData)
    const Http = new XMLHttpRequest();
    const url = 'https://script.google.com/macros/s/AKfycbyc9bb49ol9O_R6zZz6c8SnouMS6_NrVS79DHtTi5Mb0KP6sds/exec';
    Http.open("POST", url);
    Http.send(plainRequestData);
    Http.onreadystatechange = (e) => {
        try {
            let readyState = e.currentTarget.readyState
            if (readyState != 4) {
                console.log('Ainda não concluido. Status: ')
                console.log(readyState)
                return 'Not done yet'
            }
            let responseInit = Http.responseText.substring(0, 50)
            console.log(responseInit)
            console.log(Http.responseURL)

            if (responseInit.includes('Empty JSON string') || Http.responseText === '{"sessions":[""]}') {
                clearSessionsDiv()

                let sessionsDiv = document.getElementsByClassName('sessionsDiv')[0]

                let message = document.createElement('message')
                message.className = 'noSession'
                message.innerText = "No session saved so far"

                sessionsDiv.appendChild(message)

            } else if (responseInit.includes('Authorization needed')) {
                clearSessionsDiv()
                console.log(e)

                let sessionsDiv = document.getElementsByClassName('sessionsDiv')[0]

                let message = document.createElement('message')
                message.className = 'noSession'
                message.innerText = "It seems that you haven't authorized the extension yet"

                let authButton = document.createElement('Button')
                authButton.className = 'authButton'
                authButton.innerText = 'More information'
                authButton.onclick = ((e) => {
                    window.location.replace('https://sites.google.com/view/tabsmanagement/authorization')
                })

                sessionsDiv.appendChild(message)
                sessionsDiv.appendChild(authButton)
            }

            else {

                let plainSessions = JSON.parse(Http.responseText)['sessions']
                let sessions = []

                plainSessions.forEach((plain) => {
                    let genericObject = JSON.parse(plain)
                    let session = new TM_Session(new Date())
                    session.pushGenericObject(genericObject)
                    sessions.push(session)
                })

                if (!alreadyBuilt) {
                    buildSessionsCards(sessions)
                }

                alreadyBuilt = true
                serverSessions = sessions
            }
        } catch (e) {
            clearSessionsDiv()

            let sessionsDiv = document.getElementsByClassName('sessionsDiv')[0]

            let message = document.createElement('message')
            message.className = 'noSession'
            message.innerText = "An error has occurred in requesting sessions from the server. Please try again later"

            sessionsDiv.appendChild(message)
        }
    }
}

function changeParentBG(e, BG_color) {
    let element = e.srcElement
    element.parentNode.style.background = BG_color;
}

function getSessionName() {
    let name = ''
    try {
        name = document.getElementById('sessionName').value
    } catch (e) {
        name = 'Untitled'
    }
    return name
}

function createWindowOptions() {
    let windowOptions = document.createElement('div')
    windowOptions.className = 'windowOptions'
    windowOptions.onmouseleave = ((e) => {
        e.srcElement.style.background = 'white'
    })
    windowOptions.onmouseenter = ((e) => {
        e.srcElement.style.background = 'rgb(181, 218, 156)'
    })

    let b1 = document.createElement('button')
    b1.className = 'buttonWindowOptions'
    b1.innerText = 'Open tabs in this window'
    b1.onclick = ((e) => { openTabs(e, false) })

    let b2 = document.createElement('button')
    b2.className = 'buttonWindowOptions'
    b2.innerText = 'Open tabs in new window'
    b2.onclick = ((e) => { openTabs(e, true) })

    let b3 = document.createElement('button')
    b3.className = 'buttonWindowOptions'
    b3.innerText = 'Remove window'
    b3.onmouseenter = ((e) => { changeParentBG(e, 'rgb(218, 37, 37)') })
    b3.onmouseleave = ((e) => { changeParentBG(e, 'rgb(181, 218, 156)') })
    b3.onclick = ((e) => {
        activeSession.unsavedChanges = true
        let window = e.srcElement.parentNode.parentNode
        let windowNumber = window.getElementsByClassName('windowTitle')[0].innerText.split(' ')[1] - 1
        activeSession.windows.splice(windowNumber, 1)
        buildDashboard(activeSession)
    })

    windowOptions.appendChild(b1)
    windowOptions.appendChild(b2)
    windowOptions.appendChild(b3)

    return windowOptions
}

function openTabs(e, newWindow) {
    let windowDiv = e.srcElement.parentNode.parentNode
    let tabs = windowDiv.getElementsByClassName('tab')

    let URLs = []

    for (let tab of tabs) {
        let url = tab.getElementsByClassName('tabURL')[0]
        URLs.push(url.innerText)
    }

    if (newWindow) {
        let objecData = {
            'url': URLs
        }

        chrome.windows.create(objecData)
    } else {
        URLs.forEach((url) => {
            let createProperties = {
                'url': url
            }

            chrome.tabs.create(createProperties)
        })

    }
}

function getWindowNumber(e) {
    let pageTitle = e.srcElement.parentNode.parentNode.parentNode.getElementsByClassName('windowTitle')[0].innerText
    let pageNumber = +pageTitle.split(' ')[1] - 1

    return pageNumber
}

initPage()
activeSession = scanCurrentSession()
getAllSessions()

// Este ponto precisa ser melhorado!!!
// Estudar como fazer isso com promises
setTimeout(() => {
    buildDashboard(activeSession)
}, 200)

