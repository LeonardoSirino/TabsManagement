var TM_Session = /** @class */ (function () {
    function TM_Session(date) {
        this.date = date;
        this.name = 'Untitled';
        this.windows = new Array();
        this.hash = null;
        this.unsavedChanges = false;
    }
    TM_Session.prototype.replaceWindows = function (originSession) {
        this.unsavedChanges = true;
        this.windows = originSession.windows;
    };
    TM_Session.prototype.pushGenericObject = function (generic) {
        var _this = this;
        this.date = generic.date;
        this.name = generic.name;
        this.hash = generic.hash;
        var k = 1;
        generic.windows.forEach(function (window) {
            var newWindow = new TM_Window(k);
            window.tabs.forEach(function (tab) {
                newWindow.addTab(tab);
            });
            _this.addWindow(newWindow);
            k++;
        });
    };
    TM_Session.prototype.addWindow = function (window) {
        this.windows.push(window);
    };
    TM_Session.prototype.openTabs = function (mode) {
        if (mode === '1') {
            this.windows.forEach(function (window) {
                window.tabs.forEach(function (tab) {
                    var createProperties = {
                        'url': tab.url
                    };
                    chrome.tabs.create(createProperties);
                });
            });
        }
        else if (mode === '2') {
            var URLs_1 = [];
            this.windows.forEach(function (window) {
                window.tabs.forEach(function (tab) {
                    URLs_1.push(tab.url);
                });
            });
            var objecData = {
                'url': URLs_1
            };
            chrome.windows.create(objecData);
        }
        else if (mode === '3') {
            this.windows.forEach(function (window) {
                var URLs = [];
                window.tabs.forEach(function (tab) {
                    URLs.push(tab.url);
                });
                var objecData = {
                    'url': URLs
                };
                chrome.windows.create(objecData);
            });
        }
    };
    TM_Session.prototype.render = function (containerDiv) {
        var iW = 0;
        this.windows.forEach(function (window) {
            var tabs = window.tabs;
            var windowOptions = createWindowOptions();
            var windowDiv = document.createElement('div');
            windowDiv.className = 'window';
            var windowTitleDiv = document.createElement('div');
            windowTitleDiv.className = 'windowTitle';
            windowTitleDiv.innerText = 'Window ' + (+iW + 1);
            iW++;
            windowDiv.appendChild(windowTitleDiv);
            for (var iT in tabs) {
                var tab = tabs[iT];
                var tabDiv = document.createElement('a');
                tabDiv.className = 'tab';
                var tabIcon = document.createElement('img');
                tabIcon.className = 'icon';
                if (tab.icon !== "") {
                    tabIcon.setAttribute('src', tab.icon);
                }
                else {
                    tabIcon.setAttribute('src', "../images/icon.png");
                    console.log('Imagem não existe');
                    console.log("../images/icon.png");
                }
                var tabTitle = document.createElement('a');
                tabTitle.className = 'tabTitle';
                tabTitle.innerText = tab.title;
                var tabURL = document.createElement('a');
                tabURL.className = 'tabURL';
                tabURL.innerText = tab.url;
                var tabTitleandURL = document.createElement('a');
                tabTitleandURL.className = 'tabTitleandURL';
                tabTitleandURL.appendChild(tabTitle);
                tabTitleandURL.appendChild(tabURL);
                tabTitleandURL.onclick = (function (e) {
                    console.log(e); // Identificar como observar os eventos com os outros botões do mouse
                    var element = e.srcElement;
                    var k = 0;
                    while (element.className !== 'tabTitleandURL') {
                        element = element.parentNode;
                        k++;
                        if (k > 10) {
                            break;
                        }
                    }
                    var url = element.getElementsByClassName('tabURL')[0].innerText;
                    var createProperties = {
                        'url': url,
                        'active': false
                    };
                    chrome.tabs.create(createProperties);
                });
                var deleteDiv = document.createElement('button');
                deleteDiv.className = 'deleteDiv';
                deleteDiv.innerText = 'X';
                deleteDiv.onclick = (function (e) {
                    var parent = e.srcElement.parentNode;
                    var url = parent.getElementsByClassName('tabTitleandURL')[0].getElementsByClassName('tabURL')[0].innerText;
                    var windowName = parent.parentNode.getElementsByClassName('windowTitle')[0].innerText;
                    removeTab(activeSession, windowName, url);
                    console.log(activeSession);
                    buildDashboard(activeSession);
                });
                tabDiv.appendChild(deleteDiv);
                tabDiv.appendChild(tabIcon);
                tabDiv.appendChild(tabTitleandURL);
                tabDiv.appendChild(document.createElement('br'));
                document.getElementsByClassName;
                tabDiv.onmouseenter = (function (e) {
                    var parent = e.srcElement;
                    parent.getElementsByClassName('deleteDiv')[0].style.display = 'block';
                });
                tabDiv.onmouseleave = (function (e) {
                    var parent = e.srcElement;
                    parent.getElementsByClassName('deleteDiv')[0].style.display = 'none';
                });
                windowDiv.appendChild(tabDiv);
            }
            windowDiv.appendChild(windowOptions);
            containerDiv.appendChild(windowDiv);
        });
    };
    TM_Session.prototype.saveToCloud = function () {
        if (this.hash == null) {
            this.hash = (+new Date).toString(36);
        }
        this.unsavedChanges = false;
        this.name = getSessionName();
        this.date = new Date();
        var requestData = {
            'operation': 'saveSession',
            'data': JSON.stringify(this)
        };
        console.log('Salvando sessão');
        console.log(this);
        clearSessionsDiv();
        var sessionsDiv = document.getElementsByClassName('sessionsDiv')[0];
        var loadingGif = document.createElement('iframe');
        loadingGif.className = 'loadingGif';
        loadingGif.setAttribute('src', '../images/loading.gif');
        sessionsDiv.appendChild(loadingGif);
        var plainRequestData = JSON.stringify(requestData);
        var Http = new XMLHttpRequest();
        var url = 'https://script.google.com/macros/s/AKfycbyc9bb49ol9O_R6zZz6c8SnouMS6_NrVS79DHtTi5Mb0KP6sds/exec';
        Http.open("POST", url);
        Http.send(plainRequestData);
        Http.onreadystatechange = function (e) {
            var readyState = e.currentTarget.readyState;
            if (readyState != 4) {
                console.log('Ainda não concluido. Status: ');
                console.log(readyState);
                return 'Not done yet';
            }
            alreadyBuilt = false;
            getAllSessions(); // A construção do painel de sessões já é solicitada na execução dessa função
            markFirstSession = true; // O painel é construido de maneira assincrona, por isso é definido um flag para marcar a primeira sessão
        };
    };
    TM_Session.prototype.delete = function () {
        var requestData = {
            'operation': 'deleteSession',
            'data': JSON.stringify(this)
        };
        console.log('Deletando sessão');
        console.log(this);
        clearSessionsDiv();
        var sessionsDiv = document.getElementsByClassName('sessionsDiv')[0];
        var loadingGif = document.createElement('iframe');
        loadingGif.className = 'loadingGif';
        loadingGif.setAttribute('src', '../images/loading.gif');
        sessionsDiv.appendChild(loadingGif);
        var plainRequestData = JSON.stringify(requestData);
        var Http = new XMLHttpRequest();
        var url = 'https://script.google.com/macros/s/AKfycbyc9bb49ol9O_R6zZz6c8SnouMS6_NrVS79DHtTi5Mb0KP6sds/exec';
        Http.open("POST", url);
        Http.send(plainRequestData);
        Http.onreadystatechange = function (e) {
            var readyState = e.currentTarget.readyState;
            if (readyState != 4) {
                console.log('Ainda não concluido. Status: ');
                console.log(readyState);
                return 'Not done yet';
            }
            console.log('Payload de envio');
            console.log(plainRequestData);
            getAllSessions();
        };
    };
    return TM_Session;
}());
var TM_Window = /** @class */ (function () {
    function TM_Window(id) {
        this.id = id;
        this.tabs = new Array();
    }
    TM_Window.prototype.addTab = function (tab) {
        this.tabs.push(tab);
    };
    return TM_Window;
}());
var activeSession = new TM_Session(new Date());
var serverSessions = [];
var markFirstSession = false;
var alreadyBuilt = false;
function scanCurrentSession() {
    var session = new TM_Session(new Date());
    // let myWindows = []
    chrome.windows.getAll({ populate: true }, function (windows) {
        windows.forEach(function (window) {
            // let myTabs = new Array()
            var myWindow = new TM_Window(window.id);
            window.tabs.forEach(function (tab) {
                var myTab = {
                    'title': tab.title,
                    'url': tab.url,
                    'icon': tab.favIconUrl,
                    'status': tab.status
                };
                if (tab.title !== 'TabsManagement') {
                    myWindow.addTab(myTab);
                }
            });
            session.addWindow(myWindow);
        });
    });
    return session;
}
function initPage() {
    // Evento para identificar saída da página sem salvar
    window.addEventListener('beforeunload', function (e) {
        console.log(e);
        if (activeSession.unsavedChanges) {
            e.preventDefault();
            e.returnValue = '';
        }
    });
    var mainDiv = document.createElement('div');
    mainDiv.className = 'mainDiv';
    var sessionsDiv = document.createElement('div');
    sessionsDiv.className = 'sessionsDiv';
    var allWindowsDiv = document.createElement('div');
    allWindowsDiv.className = 'allWindowsDiv';
    mainDiv.appendChild(sessionsDiv);
    mainDiv.appendChild(allWindowsDiv);
    document.body.appendChild(mainDiv);
    clearSessionsDiv();
    var loadingGif = document.createElement('iframe');
    loadingGif.className = 'loadingGif';
    loadingGif.setAttribute('src', '../images/loading.gif');
    sessionsDiv.appendChild(loadingGif);
}
function clearSessionsDiv() {
    // Falta limpar o botão de autorização
    var sessionsDiv = document.getElementsByClassName('sessionsDiv')[0];
    sessionsDiv.innerHTML = '';
    var logoDiv = document.createElement('img');
    logoDiv.setAttribute('src', '../images/logo_text.png');
    logoDiv.className = 'logoDiv';
    logoDiv.onclick = function (e) {
        var createProperties = {
            'url': 'https://gitlab.com/LeonardoSirino/TabsManagement'
        };
        chrome.tabs.create(createProperties);
    };
    sessionsDiv.appendChild(logoDiv);
}
function buildSessionsCards(sessions) {
    clearSessionsDiv();
    if (sessions.length !== 0) {
        var sessionsDiv = document.getElementsByClassName('sessionsDiv')[0];
        var sessionsCards_1 = document.createElement('div');
        sessionsCards_1.className = 'sessionsCardsContainer';
        sessions.forEach(function (session) {
            var sessionCard = document.createElement('div');
            sessionCard.className = 'cardSession';
            var sessioNameDiv = document.createElement('div');
            sessioNameDiv.className = 'cardName';
            sessioNameDiv.innerText = session.name;
            var sessionCardStats = document.createElement('div');
            sessionCardStats.className = 'cardStats';
            var sessionCardNumbers = document.createElement('div');
            sessionCardNumbers.className = 'cardNumbers';
            var numWindows = session.windows.length;
            var numTabs = 0;
            session.windows.forEach(function (window) {
                numTabs += window.tabs.length;
            });
            sessionCardNumbers.innerText = numWindows + ' windows\n' + numTabs + ' tabs';
            var sessionCardDate = document.createElement('div');
            sessionCardDate.className = 'cardDate';
            var sessionDate = new Date(session.date);
            sessionCardDate.innerText = sessionDate.toLocaleDateString('pt-BR');
            sessionCard.appendChild(sessioNameDiv);
            sessionCardStats.appendChild(sessionCardNumbers);
            sessionCardStats.appendChild(sessionCardDate);
            sessionCard.appendChild(sessionCardStats);
            var hashDiv = document.createElement('div');
            hashDiv.className = 'hashDiv';
            hashDiv.innerText = session.hash;
            hashDiv.style.display = 'None';
            sessionCard.appendChild(hashDiv);
            sessionCard.onclick = function (e) {
                if (activeSession.unsavedChanges) {
                    var aux = confirm("There are unsaved changes on the open session. If you leave, this changes won't be saved. Do you really want to leave?");
                    if (aux) {
                        console.log('Alterações descartadas');
                    }
                    else {
                        return null;
                    }
                }
                var element = e.srcElement;
                var k = 0;
                while (element.className !== 'cardSession') {
                    element = element.parentNode;
                    k++;
                    if (k > 10) {
                        break;
                    }
                }
                try {
                    var previousSelected = element.parentNode.getElementsByClassName('cardSessionSelected')[0];
                    previousSelected.className = 'cardSession';
                }
                catch (_a) {
                    console.log('Erro no previousSelected');
                }
                element.className = 'cardSessionSelected';
                var hash = element.getElementsByClassName('hashDiv')[0].innerText;
                var sessionData = getSessionbyHash(hash);
                buildDashboard(sessionData);
            };
            sessionsCards_1.appendChild(sessionCard);
        });
        sessionsDiv.appendChild(sessionsCards_1);
    }
    else {
        var sessionsDiv = document.getElementsByClassName('sessionsDiv')[0];
        var message = document.createElement('message');
        message.className = 'noSession';
        message.innerText = "No session saved so far";
        sessionsDiv.appendChild(message);
    }
    if (markFirstSession) {
        // Set do foco na sessão recém salva
        try {
            var previousSelected = document.getElementsByClassName('cardSessionSelected')[0];
            previousSelected.className = 'cardSession';
        }
        catch (_a) {
            console.log('Erro no previousSelected');
        }
        console.log('Tentando setar a classe do card selecionado');
        console.log(document.getElementsByClassName('cardSession').item(0));
        document.getElementsByClassName('cardSession').item(0).className = 'cardSessionSelected';
        markFirstSession = false;
    }
}
function buildDashboard(session) {
    activeSession = session;
    var allWindowsDiv = document.getElementsByClassName('allWindowsDiv')[0];
    allWindowsDiv.innerHTML = '';
    if (activeSession.windows.length === 0) {
        var emptyText = document.createElement('div');
        emptyText.className = 'emptyText';
        emptyText.innerText = 'There is no windows in this session, please select other';
        allWindowsDiv.appendChild(emptyText);
        return;
    }
    var windows = session.windows;
    var name = session.name;
    if (!name) {
        name = 'Untitled';
    }
    var header = document.createElement('div');
    header.className = 'dashHeader';
    var sessionNameHolder = document.createElement('form');
    sessionNameHolder.setAttribute('autocomplete', 'off');
    var sesssionName = document.createElement('input');
    sesssionName.setAttribute('autocomplete', 'false');
    sesssionName.setAttribute('name', 'hidden');
    sesssionName.onchange = (function (e) {
        activeSession.unsavedChanges = true;
    });
    sesssionName.id = 'sessionName';
    sesssionName.value = name;
    sessionNameHolder.appendChild(sesssionName);
    header.appendChild(sessionNameHolder);
    var saveButton = document.createElement('button');
    saveButton.className = 'headerButton';
    saveButton.innerText = 'Save';
    saveButton.onclick = (function () { activeSession.saveToCloud(); });
    var delButton = document.createElement('button');
    delButton.className = 'headerButton';
    delButton.innerText = 'Delete';
    delButton.onclick = (function (e) {
        var aux = confirm('Do you really want to delete this session?');
        if (aux)
            [
                activeSession.delete()
            ];
    });
    var overwriteButton = document.createElement('button');
    overwriteButton.className = 'headerButton';
    overwriteButton.innerText = 'Overwrite with current';
    overwriteButton.onclick = (function () {
        var currentSession = scanCurrentSession();
        // Também estudar como remover esse timeout
        setTimeout(function () {
            activeSession.replaceWindows(currentSession);
            buildDashboard(activeSession);
        }, 200);
    });
    var openButton = document.createElement('button');
    openButton.className = 'headerButton';
    openButton.innerText = 'Open';
    var dropMenu = document.createElement('div');
    dropMenu.className = 'headerDropMenu';
    dropMenu.appendChild(openButton);
    var dropMenuContent = document.createElement('div');
    dropMenuContent.className = 'dropMenuContent';
    var dmb1 = document.createElement('Button');
    dmb1.className = 'dropMenuItem';
    dmb1.innerText = 'Open tabs\nin this window';
    dmb1.onclick = (function (e) {
        console.log(activeSession);
        activeSession.openTabs('1');
    });
    var dmb2 = document.createElement('Button');
    dmb2.className = 'dropMenuItem';
    dmb2.innerText = 'Open tabs\nin new window';
    dmb2.onclick = (function (e) {
        console.log(activeSession);
        activeSession.openTabs('2');
    });
    var dmb3 = document.createElement('Button');
    dmb3.className = 'dropMenuItem';
    dmb3.innerText = 'Open tabs\nin respective windows';
    dmb3.onclick = (function (e) {
        console.log(activeSession);
        activeSession.openTabs('3');
    });
    dropMenuContent.appendChild(dmb1);
    dropMenuContent.appendChild(dmb2);
    dropMenuContent.appendChild(dmb3);
    dropMenu.appendChild(dropMenuContent);
    var headerButtonsHolder = document.createElement('div');
    headerButtonsHolder.className = 'headerButtonsHolder';
    headerButtonsHolder.appendChild(dropMenu);
    headerButtonsHolder.appendChild(saveButton);
    headerButtonsHolder.appendChild(delButton);
    headerButtonsHolder.appendChild(overwriteButton);
    header.appendChild(headerButtonsHolder);
    allWindowsDiv.appendChild(header);
    var hashDiv = document.createElement('div');
    hashDiv.innerText = session.hash;
    hashDiv.style.display = 'None';
    allWindowsDiv.appendChild(hashDiv);
    session.render(allWindowsDiv);
}
function getSessionbyHash(hash) {
    var matched = new TM_Session(new Date());
    serverSessions.forEach(function (e) {
        if (e.hash === hash) {
            matched = e;
        }
    });
    return matched;
}
function removeTab(session, windowName, url) {
    session.unsavedChanges = true;
    var j = 0;
    session.windows.forEach(function (window) {
        var k = 0;
        window.tabs.forEach(function (tab) {
            // Tirar isso daqui
            var data = {
                curName: window.id,
                searchName: windowName,
                curURL: tab.url,
                searchURL: url
            };
            console.table(data);
            if (tab.url === url) {
                var aux = window.tabs.splice(k, 1);
                console.log(aux);
            }
            k++;
        });
        if (window.tabs.length === 0) {
            session.windows.splice(j, 1);
        }
        j++;
    });
}
function getAllSessions() {
    var requestData = {
        'operation': 'getAllSesssions',
        'data': ''
    };
    var alreadyBuilt = false;
    var plainRequestData = JSON.stringify(requestData);
    var Http = new XMLHttpRequest();
    var url = 'https://script.google.com/macros/s/AKfycbyc9bb49ol9O_R6zZz6c8SnouMS6_NrVS79DHtTi5Mb0KP6sds/exec';
    Http.open("POST", url);
    Http.send(plainRequestData);
    Http.onreadystatechange = function (e) {
        try {
            var readyState = e.currentTarget.readyState;
            if (readyState != 4) {
                console.log('Ainda não concluido. Status: ');
                console.log(readyState);
                return 'Not done yet';
            }
            var responseInit = Http.responseText.substring(0, 50);
            console.log(responseInit);
            console.log(Http.responseURL);
            if (responseInit.includes('Empty JSON string') || Http.responseText === '{"sessions":[""]}') {
                clearSessionsDiv();
                var sessionsDiv = document.getElementsByClassName('sessionsDiv')[0];
                var message = document.createElement('message');
                message.className = 'noSession';
                message.innerText = "No session saved so far";
                sessionsDiv.appendChild(message);
            }
            else if (responseInit.includes('Authorization needed')) {
                clearSessionsDiv();
                console.log(e);
                var sessionsDiv = document.getElementsByClassName('sessionsDiv')[0];
                var message = document.createElement('message');
                message.className = 'noSession';
                message.innerText = "It seems that you haven't authorized the extension yet";
                var authButton = document.createElement('Button');
                authButton.className = 'authButton';
                authButton.innerText = 'More information';
                authButton.onclick = (function (e) {
                    window.location.replace('https://sites.google.com/view/tabsmanagement/authorization');
                });
                sessionsDiv.appendChild(message);
                sessionsDiv.appendChild(authButton);
            }
            else {
                var plainSessions = JSON.parse(Http.responseText)['sessions'];
                var sessions_1 = [];
                plainSessions.forEach(function (plain) {
                    var genericObject = JSON.parse(plain);
                    var session = new TM_Session(new Date());
                    session.pushGenericObject(genericObject);
                    sessions_1.push(session);
                });
                if (!alreadyBuilt) {
                    buildSessionsCards(sessions_1);
                }
                alreadyBuilt = true;
                serverSessions = sessions_1;
            }
        }
        catch (e) {
            clearSessionsDiv();
            var sessionsDiv = document.getElementsByClassName('sessionsDiv')[0];
            var message = document.createElement('message');
            message.className = 'noSession';
            message.innerText = "An error has occurred in requesting sessions from the server. Please try again later";
            sessionsDiv.appendChild(message);
        }
    };
}
function changeParentBG(e, BG_color) {
    var element = e.srcElement;
    element.parentNode.style.background = BG_color;
}
function getSessionName() {
    var name = '';
    try {
        name = document.getElementById('sessionName').value;
    }
    catch (e) {
        name = 'Untitled';
    }
    return name;
}
function createWindowOptions() {
    var windowOptions = document.createElement('div');
    windowOptions.className = 'windowOptions';
    windowOptions.onmouseleave = (function (e) {
        e.srcElement.style.background = 'white';
    });
    windowOptions.onmouseenter = (function (e) {
        e.srcElement.style.background = 'rgb(181, 218, 156)';
    });
    var b1 = document.createElement('button');
    b1.className = 'buttonWindowOptions';
    b1.innerText = 'Open tabs in this window';
    b1.onclick = (function (e) { openTabs(e, false); });
    var b2 = document.createElement('button');
    b2.className = 'buttonWindowOptions';
    b2.innerText = 'Open tabs in new window';
    b2.onclick = (function (e) { openTabs(e, true); });
    var b3 = document.createElement('button');
    b3.className = 'buttonWindowOptions';
    b3.innerText = 'Remove window';
    b3.onmouseenter = (function (e) { changeParentBG(e, 'rgb(218, 37, 37)'); });
    b3.onmouseleave = (function (e) { changeParentBG(e, 'rgb(181, 218, 156)'); });
    b3.onclick = (function (e) {
        activeSession.unsavedChanges = true;
        var window = e.srcElement.parentNode.parentNode;
        var windowNumber = window.getElementsByClassName('windowTitle')[0].innerText.split(' ')[1] - 1;
        activeSession.windows.splice(windowNumber, 1);
        buildDashboard(activeSession);
    });
    windowOptions.appendChild(b1);
    windowOptions.appendChild(b2);
    windowOptions.appendChild(b3);
    return windowOptions;
}
function openTabs(e, newWindow) {
    var windowDiv = e.srcElement.parentNode.parentNode;
    var tabs = windowDiv.getElementsByClassName('tab');
    var URLs = [];
    for (var _i = 0, tabs_1 = tabs; _i < tabs_1.length; _i++) {
        var tab = tabs_1[_i];
        var url = tab.getElementsByClassName('tabURL')[0];
        URLs.push(url.innerText);
    }
    if (newWindow) {
        var objecData = {
            'url': URLs
        };
        chrome.windows.create(objecData);
    }
    else {
        URLs.forEach(function (url) {
            var createProperties = {
                'url': url
            };
            chrome.tabs.create(createProperties);
        });
    }
}
function getWindowNumber(e) {
    var pageTitle = e.srcElement.parentNode.parentNode.parentNode.getElementsByClassName('windowTitle')[0].innerText;
    var pageNumber = +pageTitle.split(' ')[1] - 1;
    return pageNumber;
}
initPage();
activeSession = scanCurrentSession();
getAllSessions();
// Este ponto precisa ser melhorado!!!
// Estudar como fazer isso com promises
setTimeout(function () {
    buildDashboard(activeSession);
}, 200);
//# sourceMappingURL=dashboard.js.map