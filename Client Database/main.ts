let database = getDatabaseFile()

function doGet(e){
    return HtmlService.createHtmlOutputFromFile('index')
}

function doPost(e) {
    let cache = CacheService.getScriptCache()
    let plainRequestData = e.postData.contents
    let requestData = JSON.parse(plainRequestData)
    let operation = requestData.operation
    let data = requestData.data

    cache.put('plainRequestData', plainRequestData)
    cache.put('requestData', requestData)
    cache.put('operation', operation)
    cache.put('data', data)

    let parsedSession = ''

    try {
        parsedSession = JSON.parse(data)
    } catch{
    }

    cache.put('parsedSession', parsedSession)

    let response: string

    if (operation === 'saveSession') {
        saveSession(parsedSession)
        response = 'ok'
        sortSessions()
    } else if (operation === 'getAllSesssions') {
        let sessions = {
            'sessions': getAllSessions()
        }

        response = JSON.stringify(sessions)
        cache.put('sessions', response)
    } else if (operation === 'deleteSession') {
        deleteSession(parsedSession)
        response = 'ok'
    } else {
        Logger.log('Operação não reconhecida')
    }

    return ContentService.createTextOutput(response).setMimeType(ContentService.MimeType.JAVASCRIPT);
}

function readCache() {
    let cache = CacheService.getScriptCache()
    Logger.log(cache.get('sessions'))
    Logger.log(cache.get('plainRequestData'))
    Logger.log(cache.get('requestData'))
    Logger.log(cache.get('operation'))
    Logger.log(cache.get('data'))
    Logger.log(cache.get('parsedSession'))
}

function deleteSession(session) {
    try {
        let sessions = getAllSessions()

        let k = 1
        sessions.forEach((e) => {
            let parsed = JSON.parse(e)
            if (parsed.hash === session.hash) {
                database.deleteRow(k)
            }
            k++
        })
        Logger.log('Sessão deletada')
    } catch {
        Logger.log('Sessão não deletada')
    }
}

function saveSession(session) {
    deleteSession(session)
    let database = getDatabaseFile()
    database.appendRow([JSON.stringify(session), session.date])

    getInfos(session)
}

function getInfos(session) {
    let numTabs = 0
    let numWindows = 0
    session.windows.forEach((window) => {
        numTabs += window.tabs.length
        numWindows++
    })

    let user = Session.getActiveUser()

    let info = {
        'numTabs': numTabs,
        'numWindows': numWindows,
        'userEmail': user.getEmail(),
        'userID': user.getUserLoginId(),
        'userLocale': Session.getActiveUserLocale(),
        'scriptTimeZone': Session.getScriptTimeZone(),
        'timeZone': Session.getTimeZone()
    }

    postInfo(info)

    return info
}

function postInfo(info) {
    let url = 'https://script.google.com/macros/s/AKfycbzP_DzFv6akZpKGJGinnp9SlBKze2_GAiGr_UToXWvFeAC2p3E/exec'
    let options = {
        'method': 'post',
        'contentType': 'application/json',
        'payload': JSON.stringify(info)
    };
    UrlFetchApp.fetch(url, options);
}

function getDatabaseFile() {
    let root = DriveApp.getRootFolder()
    let sheetFile: GoogleAppsScript.Drive.File

    try {
        sheetFile = root.getFoldersByName("TabsManagement").next().getFilesByName("database").next()
    } catch (error) {
        let folder = root.createFolder("TabsManagement")
        let ss = SpreadsheetApp.create('database')
        let temp = DriveApp.getFileById(ss.getId())
        folder.addFile(temp)
        root.removeFile(temp)

        sheetFile = root.getFoldersByName("TabsManagement").next().getFilesByName("database").next()
    }

    let spreadSheet = SpreadsheetApp.openById(sheetFile.getId())

    return spreadSheet
}

function getAllSessions() {
    let aux = database.getDataRange().getValues()
    let sessions = []

    aux.forEach((Element) => {
        sessions.push(Element[0])
    })

    return sessions
}

function sortSessions(){
    database.getDataRange().sort({column: 2, ascending: false})
}