let sps = SpreadsheetApp.getActive()

function doPost(e) {
    let cache = CacheService.getScriptCache()
    let plainRequestData = e.postData.contents
    let data = JSON.parse(plainRequestData)

    let usersSheet = sps.getSheetByName('Users')

    let lines = usersSheet.getDataRange().getHeight()
    let aux = usersSheet.getRange(2, 1, lines).getValues()

    let emails = []
    aux.forEach((line) => {
        emails.push(line[0])
    })

    Logger.log(emails)

    let indexMatch = emails.indexOf(data.userEmail)
    Logger.log(indexMatch)

    if (indexMatch === -1) {
        // Usuário ainda não existente, adicionando nova linha
        let rowData = [data.userEmail, data.userID, data.userLocale, data.scriptTimeZone, data.timeZone, 1, data.numWindows, data.numTabs]
        usersSheet.appendRow(rowData)
    } else {
        let row = indexMatch + 2
        let column = 6
        let valuesRange = usersSheet.getRange(row, column, 1, 3)

        let values = valuesRange.getValues()[0]
        values[0]++
        values[1] += data.numWindows
        values[2] += data.numTabs

        valuesRange.setValues([values])
    }
    let histSheet = sps.getSheetByName('Usage')
    let height = histSheet.getLastRow()

    let valuesRange = histSheet.getRange(height, 2, 1, 3)
    let values = valuesRange.getValues()[0]
    values[0]++
    values[1] += data.numWindows
    values[2] += data.numTabs

    valuesRange.setValues([values])
}

function nextTimeWindow() {
    let histSheet = sps.getSheetByName('Usage')
    let values = [new Date(), 0, 0, 0]
    histSheet.appendRow(values)
}

function tester() {
    let contents = {
        'userEmail': 'teste'
    }

    let postData = {
        'contents': JSON.stringify(contents)
    }

    let testerData = {
        'postData': postData
    }

    doPost(testerData)
}